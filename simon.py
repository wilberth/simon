#!/usr/bin/env python3
import sys
import random
import time
from psychopy import core, visual, event
from rusocsci import buttonbox
from rewards import rewards

## constants
circleRadius = 0.1             # in units of screen height
circleDistance = 0.2           # from center in units of screen height
instructionTextHeight = 0.03   # in units of screen height
feedbackTextHeight = 0.06      # in units of screen height
normalResponseDeadline = 0.500 # maximum time for response in seconds
fastResponseDeadline = 0.250   # maximum time for response in seconds in fast trials
fixationDuration = 0.500       # fixation cross time in s
itiMin = 0.750                 # minimum inter trial interval (in which feedback is shown) in s
itiMax = 1.250                 # maximum inter trial interval (in which feedback is shown) in s
fastPart = 0.20                # part of trials that is fast
congruentPart = 0.75           # part of trials that is congruent
nPracticeTrials = 10           # number of trials before expriment starts
experimentDuration = 15#*60     # time in seconds
keys = ['e', 'i']              # left and right keys
useButtonbox = False           # use the buttonbox: True: buttonbox/ False: keyboard
#keys = ['C', 'G']              # left and right buttons for buttonbox
#useButtonbox = True            # use the buttonbox: True: buttonbox/ False: keyboard
dataFile = sys.stdout          # where is data written


## initialization
ppn = int(sys.argv[1])
#dataFile = open("data_{:d}.csv".format(ppn), "wb")

# 50% swaps keys
swappedKeys = keys
#keySwap = random.randint(0, 1) 
keySwap = (ppn//3)%2

if keySwap:
	swappedKeys = keys[1], keys[0]
# keys[0] is the left hand response, keys[1] is the right response
# swappedKeys[0] is the blue response, swappedKeys[1] is the green response

#rewardCategory = random.randint(0, 2) 
rewardCategory = ppn%3

if(useButtonbox):
	bb = buttonbox.Buttonbox()
win = visual.Window([1200, 900], monitor="testMonitor", units="height", color="black", fullscr=False)
myMouse = event.Mouse(visible = False)

## function definition  
def waitKeys(maxWait=float("inf")):
	" uses either the buttonbox or the keyboard, return None or letter "
	if(useButtonbox):
		# only button presses, not button releases
		k = bb.waitButtons(maxWait=maxWait, buttonList=('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'))
	else:
		k = event.waitKeys(maxWait=maxWait)
	if k:
		return k[0]
	return k



## stimuli
text = """Welcome. Today you will be playing a game where you can earn some extra money. The more points you earn in the game, the more money you will be paid as part of your bonus.

Your task is simple. Every time you see a circle you need to press the correct button:

 When you see a BLUE circle, press the {:s} button
 When you see a GREEN circle, press the {:s} button

To do this task successfully you need to IGNORE the position of the circle on the screen. You will not have very much time so you need to respond as accurately and quickly as possible. 

You should put your left index finger on the {:s} button and your right index finger on the {:s} button. 

Let's practice for a little bit.""".format(*swappedKeys, *keys)
instructions1 = visual.TextStim(win, text=text, wrapWidth=1.2, height=instructionTextHeight)

# post practise
text = """Good. You probably noticed that the game moves quickly and you don't have long to respond! Now you're going to play this game for a little while

REMEMBER:
  When you see a BLUE circle, press the {:s} button
  When you see a GREEN circle, press the {:s} button

Remember, the more CORRECT responses you make in this game, the more REAL BONUS MONEY you will be paid on top of the normal payment.
""".format(*keys)
instructions2 = visual.TextStim(win, text=text, wrapWidth=1.2, height=instructionTextHeight)

fixation = visual.TextStim(win, text="+", height=.1)
  
blueLeft = visual.Circle(win, pos=(-circleDistance, 0), radius=circleRadius, edges=256, fillColor = (-1, -1, 1))
greenLeft = visual.Circle(win, pos=(-circleDistance, 0), radius=circleRadius, edges=256, fillColor = (-1, 0, -1))
blueRight = visual.Circle(win, pos=(circleDistance, 0), radius=circleRadius, edges=256, fillColor = (-1, -1, 1))
greenRight = visual.Circle(win, pos=(circleDistance, 0), radius=circleRadius, edges=256, fillColor = (-1, 0, -1))
stimuli = (blueLeft, greenLeft, blueRight, greenRight)

## trial definition
maxReward = 0
totalReward = 0
def trial(phase, iTrial, reward):
	global maxReward, totalReward
	# fixation
	fixation.draw()
	win.flip()
	core.wait(fixationDuration)
	
	# prime (reward)
	prime = visual.TextStim(win, text="{:2d}".format(reward), height=.1)
	prime.draw()
	win.flip()
	core.wait(1.000)

	# determine stimulus
	congruent = False
	if random.random() < congruentPart:
		congruent = True

	if congruent != keySwap:
		# congruent but not keySwap, keySwap but not congruent
		stimulus = random.choice((0,3)) # blueLeft, greenRight
	else:
		stimulus = random.choice((1,2)) # greenLeft, blueRight
	
	# determine response deadline
	responseDeadline = normalResponseDeadline
	if random.random() < fastPart:
		responseDeadline = fastResponseDeadline

	# show stimulus
	stimuli[stimulus].draw()
	win.flip()
	stimulusTime = time.time() - t0
	response = waitKeys(responseDeadline)
	responseTime = time.time() - stimulusTime - t0
	
	# feedback
	if response==None:
		feedback = visual.TextStim(win, text="TOO SLOW", color="orange", bold=True,
			height=feedbackTextHeight, pos=(0, -0.2))
		correct = "Slow"
	elif response not in keys:
		feedback = visual.TextStim(win, 
			text="WRONG KEYS\nuse '{:}' and '{:}' buttons".format(*keys), 
			color="lime",
			height=feedbackTextHeight, pos=(0, -0.2))
		correct = "Wrong"
	else:
		# stimulus%2 is true if stimulus was green
		correct = stimulus%2 == (response==swappedKeys[1]) # True if response is correct

		if correct:
			feedback = visual.TextStim(win, text="you win\n{:d}".format(reward), color="lime", height=feedbackTextHeight)
		else:
			feedback = visual.TextStim(win, text="X\nINCORRECT", color="green", 
				height=feedbackTextHeight)
			
	# data
	feedback.draw()
	win.flip()
	iti = random.uniform(itiMin, itiMax)
	core.wait(iti) # iti = feedback duration
	print("{:d},{:s},{:1d},{:1b},{:4d},{:.3f},{:d},{:.3f},{:.3f},{:1b},{:1b},{:1b},{:s},{:s},{:.3f}".
		format(ppn, phase, rewardCategory, keySwap, iTrial, stimulusTime, reward, responseDeadline, responseTime, 
			stimulus//2, stimulus%2, congruent, str(response), str(correct), iti),
		file=dataFile)
	
	if phase=="experiment":
		maxReward += reward
		if correct==True:
			totalReward += reward


print("#ppn,phase,rewardCategory,keySwap, iTrial,stimulusTime,reward,responseDeadline,responseTime,right,green,congruent,response,correct,iti",
	file=dataFile)

## experiment
instructions1.draw()
win.flip()
waitKeys()

t0 = time.time()
for i in range(nPracticeTrials):
	trial("practice", i, random.randint(5, 94))

instructions2.draw()
win.flip()
waitKeys()

iTrial = 0
t0 = time.time()
while time.time() - t0 < experimentDuration:
	reward = rewards[rewardCategory][iTrial%len(rewards[rewardCategory])]
	trial("experiment", iTrial, reward)
	iTrial+=1
	
# reward information
money = 2.0*totalReward/maxReward
print("# reward: {:.2f}".format(money), file=dataFile)
text = """Thank you for participating. Your reward is € {:.2f}.""".format(money)
info = visual.TextStim(win, text=text, wrapWidth=1.2, height=instructionTextHeight)
info.draw()
win.flip()
waitKeys()
